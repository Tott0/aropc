(function(){

  angular
    .module('app')
    .controller('RightContentController', [
      'sessionService',
      RightContentController
    ]);

  function RightContentController(Session) {
    var vm = this;

    vm.options = [
      {
          icon : 'input',
          text: 'Entrar',
          state: 'signin',
          auth: 0
      },
      {
          icon : 'add_circle',
          text: 'Crear Cuenta',
          state: 'signup',
          auth: 0
      },
      {
          icon: 'person',
          text: 'Ver Perfil',
          state: 'app.page-profile',
          auth: 1
      },
      {
          icon: 'exit_to_app',
          text: 'Salir',
          state: 'signout',
          auth: 1
      }
    ];

    vm.shouldShow = function(item){
      var isAuth = Session.isAuth();
      var role   = Session.getRole();
      if(item.auth == 1){
        return isAuth;
      }else{
        return !isAuth;
      }
    }
  }

})();
