(function(){
  'use strict';

  angular.module('app')
  .factory('sessionService', ['$http', '$localStorage', 'base64', 'URL', '$rootScope', sessionService]);

  function sessionService($http, $localStorage, base64, URL, $rootScope) {
    return {
      login: function(user){
        var password = base64.encodex(user.password, 7);
        return $http({method: 'POST', url: URL.LINK + '/sessions', data: {email: user.email, password: password}})
      },
      isAuth: function(){
        try{
          return $localStorage.AropcAuth.token !== null;
        }catch(err){
          $localStorage.AropcAuth = {
            token: null,
            role: null,
            full_name: null,
            permissions: null
          }
          return false;
        }
      },
      setToken: function(token, role, full_name, permissions){
        $localStorage.AropcAuth = {
          token: token,
          role: role,
          full_name: full_name,
          permissions: permissions
        }
      },
      logout: function(){
        var token = this.getToken();
        $http({method: 'DELETE', url: URL.LINK + '/sessions', headers: {token: token}}).success(function(data){
        }).error(function (data) {
        });
        $localStorage.AropcAuth = {
          token: null,
          role: null,
          full_name: null
        }
        $rootScope.$broadcast('sessionDestroyed');
      },
      getToken: function(){
        return this.isAuth() ? $localStorage.AropcAuth.token : false;
      },
      getRole: function(){
        return 'user';
        return 'admin';
        return this.isAuth() ? $localStorage.AropcAuth.role : false;
      },
      getFullname: function(){
        return this.isAuth() ? $localStorage.AropcAuth.full_name : false;
      },
      getPermissions: function(){
        return this.isAuth() ? $localStorage.AropcAuth.permissions : false;
      }
    }
  }
})();
