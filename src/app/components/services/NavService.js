(function(){
  'use strict';

  angular.module('app')
          .service('navService', [
          '$q',
          navService
  ]);

  function navService($q){
    var menuItems = [
      {
        name: 'Dashboard',
        icon: 'dashboard',
        sref: '.dashboard'
      },
      {
        name: 'Profile',
        icon: 'person',
        sref: '.profile'
      },
      {
        name: 'Table',
        icon: 'view_module',
        sref: '.table'
      }
    ];

    var menuItemsU = [
      {
        name: 'Perfil',
        icon: 'face',
        sref: '.profile'
      },
      {
        name: 'Cuestionario',
        icon: 'assignment',
        sref: '.profile'
      },
      {
        name: 'Feedback',
        icon: 'assessment',
        sref: '.feedback'
      },
      {
        name: 'Grafica',
        icon: 'pie_chart_outlined',
        sref: '.chart'
      }
    ];

    var menuItemsA = [
      {
        name: 'Perfil',
        icon: 'face',
        sref: '.profile'
      },
      {
        name: 'Organizaciones',
        icon: 'business',
        sref: '.organizations'
      },
      {
        name: 'Feedback',
        icon: 'assessment',
        sref: '.feedback'
      },
      {
        name: 'Grafica',
        icon: 'pie_chart_outlined',
        sref: '.chart'
      }
    ];

    return {
      loadAllItems : function() {
        return $q.when(menuItems);
      }
    };
  }

})();
