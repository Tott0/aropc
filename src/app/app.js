(function(){
  'use strict';

  angular.module('app', [
      'ngMaterial'
     ,'ngStorage'
     ,'ab-base64'
   ]);

})();
